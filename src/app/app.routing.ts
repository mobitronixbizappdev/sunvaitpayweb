import { Routes, RouterModule } from '@angular/router';
import { AppLayoutComponent } from './layout/app-layout/app-layout.component';
import {DashboardComponent, UsersComponent, RolesComponent, LoginComponent} from './admin';
import {MSignupComponent, MLoginComponent, MDashboardComponent, MRolesComponent, MUsersComponent, MSettingsComponent} from './merchants';
import {AuthGuard} from './shared/guards';
import { HomeComponent } from './home/home.component';

const appRoutes: Routes =
[

    {
        path: 'admin',
        component: AppLayoutComponent,
        children:
        [
             { path: 'dashboard', component: DashboardComponent },
             { path: 'users', component: UsersComponent },
             { path: 'roles', component: RolesComponent }
         ]
         , canActivate : [AuthGuard]
    },
    {
        path: 'merchant',
        component: AppLayoutComponent,
        children:
        [
             { path: 'dashboard', component: MDashboardComponent },
             { path: 'users', component: MUsersComponent },
             { path: 'settings', component: MSettingsComponent },
             { path: 'roles', component: MRolesComponent }
         ]
         , canActivate : [AuthGuard]
    },
    // no layout routes
    { path: '', component: HomeComponent },
    { path: 'admin/login', component: LoginComponent },
    { path: 'merchant/login', component: MLoginComponent },
    { path: 'merchant/signup', component: MSignupComponent },
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes, {useHash: true});


