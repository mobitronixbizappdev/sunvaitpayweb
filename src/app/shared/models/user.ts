export class User {
    public ID: number;
    public Firstname = '';
    public Surname = '';
    public Email = '';
    public DOB: Date;
    public AccountTypeID: number;
    public AccountType: any;
    public SchoolID: number;
    public Password: string;
    public Phonenumber: string;
    public DateRegistered: Date;
    public AppDetectedAddress: string;
    public Location: string;
}
export class LoginCredentials {
    public UserName = '';
    public Password = '';
}
export class SignupCredentials {
    Country = '';
    PhoneNumber: number;
    Email = '';
    Password = '';
    ConfirmPassword = '';
}
export class ChangePassword {
    public Password = '';
    public ConfirmPassword = '';
    public NewPassword = '';
}
export class Role {
    public ID: number;
    public Name = '';
    public UserCount = 0;
    public Privilege: string [];
}
