import 'rxjs/add/operator/map';

import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../../app.constant';

@Injectable()
export class DataService {

    private actionUrl: string;
    constructor(private http: HttpClient, private _configuration: Configuration) {
    }

    public getAll<T>(actionUrl: string): Observable<T> 
    {
        return this.http.get<T>(actionUrl);
    }
    public find<T>(actionUrl: string, data: any): Observable<T> {
        return this.http.get<T>(actionUrl, { params: data });
    }
    public getSingle<T>(id: number): Observable<T> {
        return this.http.get<T>(this.actionUrl + id);
    }

    public add<T>(itemName: string): Observable<T> {
        const toAdd = JSON.stringify({ ItemName: itemName });
        return this.http.post<T>(this.actionUrl, toAdd);
    }

    public post<T>(actionUrl: string, items: any): Observable<T> 
    {
        let headers = new HttpHeaders({ "Content-type": 'application/json' });
        const toAdd = JSON.stringify(items);
        return this.http.post<T>(actionUrl, toAdd, { headers: headers });
    }
}


@Injectable()
export class CustomInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!req.headers.has('Content-Type')) {
            req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
        }

        req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
        return next.handle(req);
    }
}

