import 'rxjs/add/operator/map';
import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { User,ChangePassword } from '../models';
import { Configuration } from '../../app.constant';
import { DataService } from './data.service';
@Injectable()
export class RoleService {
    private actionUrl: string; private ApiUrl: string; private AccountsUrl: string;
    constructor(private http: HttpClient, private config: Configuration, private _data: DataService) 
    {
        this.ApiUrl = this.config.ApiUrl + 'roles/';
    }
    public GetRoles<T>(): Observable<T> 
    {
        return this._data.getAll(this.ApiUrl);
    }
    public CreateRole<T>(obj: any): Observable<T> 
    {
        return this._data.post(this.ApiUrl, obj);
    }
}