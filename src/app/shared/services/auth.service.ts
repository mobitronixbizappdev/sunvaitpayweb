import { Injectable } from '@angular/core';
import { Configuration } from '../../app.constant';
import { Observable } from 'rxjs/Observable';
import { DataService } from './data.service';
import { User, ChangePassword, ApiResponse } from '../models';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class AuthService {
    private actionUrl: string;
    private ApiUrl: string;
    private AccountsUrl: string;
    public _activeUser = new BehaviorSubject<User>(this.getStorage('currentUser'));
    ActiveUser = this._activeUser.asObservable();

    constructor(private http: HttpClient, private config: Configuration, private _data: DataService) {
        this.ApiUrl = this.config.ApiUrl + 'authenticate/';
    }
    public AdminLogin<ApiResponse>(obj: any): Observable<ApiResponse> {
        return this.http.get<ApiResponse>('../assets/mocks/admin.json');
         // return this._data.post(this.ApiUrl + 'login', obj);
    }
    public MerchantLogin<ApiResponse>(obj: any): Observable<ApiResponse> {

        return this.http.get<ApiResponse>('../assets/mocks/merchant.json');
        // return this._data.post(this.ApiUrl + 'login', obj);
    }
    public MerchantSignup<ApiResponse>(user: any): Observable<ApiResponse> {
        return this._data.post(this.ApiUrl + 'signup', user);
    }
    public ForgotPassword<T>(obj: any): Observable<T> {
        return this._data.post(this.ApiUrl + 'forgotpassword', obj);
    }
    public UpdateProfile<T>(user: any): Observable<T> {
        return this._data.post(this.ApiUrl + 'updateProfile', user);
    }
    public UpdatePassword<T>(user: ChangePassword): Observable<T> {
        return this._data.post(this.ApiUrl + 'updatePassword', user);
    }
    public isJSON(str) {
        try {
            const obj = JSON.parse(str);
            if (obj && typeof obj === 'object' && obj !== null) {
                return true;
            }
        } catch (err) { }
        return false;
    }
    private getStorage(str: string) {
        if (this.isJSON(localStorage.getItem(str))) {
            return JSON.parse(localStorage.getItem(str));
        }
    }

}


