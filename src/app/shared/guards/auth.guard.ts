import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('currentUser')) {
            if (this.isJSON(localStorage.getItem('currentUser')))
             {
                 return true;
            } else {
                this.router.navigateByUrl('/login', { queryParams: { returnUrl: state.url } });
                return false;
            }
        }
        this.router.navigateByUrl('/login', { queryParams: { returnUrl: state.url } });
        return false;
    }
    private isJSON(str) {
        try {
            var obj = JSON.parse(str);
            if (obj && typeof obj === 'object' && obj !== null) {
                return true;
            }
        } catch (err) { }
        return false;
    }
} 