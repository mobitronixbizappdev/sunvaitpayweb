import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../shared/services';
import { LoginCredentials, ApiResponse } from '../../shared/models';
declare var $; declare var iziToast;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthService]
})
export class LoginComponent implements OnInit {
  psb: any; loginData: LoginCredentials; returnUrl: string;
  constructor(private router: Router, private _auth: AuthService, private route: ActivatedRoute) {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || "admin/dashboard";
  }

  ngOnInit() {
    $("body").addClass("u-bg-dull-white")
  }

  ngOnDestroy() {
    $("body").addClass("u-bg-dull-white")
  }
  login() {
    this.psb = new Promise((resolve, reject) => {
      this._auth.AdminLogin(this.loginData)
        .subscribe
        ((data: ApiResponse) => {
          let rs: any = data;
          if (rs.success) {
            localStorage.setItem("currentUser", JSON.stringify(rs.data))
            localStorage.setItem("token", rs.Token)
            iziToast.info({
              title: 'Welcome Back',
              position: 'topCenter',
              timeout: 20000,
            });
            location.href = "#/" + this.returnUrl
            //this.router.navigateByUrl(this.returnUrl);
          }
          else {
            iziToast.error({
              title: "Invalid Username or Password",
              position: 'topCenter',
              timeout: 20000,
            });
            setTimeout(resolve, 1000);
          }
        },
        error => {
          iziToast.error({
            title: "Please try again",
            position: 'topCenter',
            timeout: 20000,
          });
          setTimeout(resolve, 1000);
        });
    });
  }
}
