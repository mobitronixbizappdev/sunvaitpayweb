import { Component, OnInit } from '@angular/core';
import { Role, ApiResponse } from '../../shared/models';
import { RoleService } from '../../shared/services/role.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {
  Roles: Role[] = []; isLoading: boolean = false;
  constructor(private _role: RoleService) {
  }

  ngOnInit() {
    this.GetRoles();
  }
  public GetRoles(PageNumber: number = 1) {
    this.isLoading = true;
    this._role.GetRoles().subscribe
      ((data: ApiResponse) => {
        if (data.success) {
          this.Roles = data.data;
        }
        else {
        }
        this.isLoading = false;
      })
  }

}
