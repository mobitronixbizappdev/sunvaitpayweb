import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RolesComponent } from './roles/roles.component';
import { UsersComponent } from './users/users.component';
import { SettingsComponent } from './settings/settings.component';
import { AuthService, RoleService } from '../shared/services';
import { LoginComponent } from './login/login.component';
import { Angular2PromiseButtonModule } from 'angular2-promise-buttons';

@NgModule({
  imports: [
    CommonModule,Angular2PromiseButtonModule.forRoot()
  ],
  declarations: [DashboardComponent, RolesComponent, UsersComponent, SettingsComponent,LoginComponent],
  exports: [DashboardComponent, RolesComponent, UsersComponent, SettingsComponent,LoginComponent],
  providers:[RoleService]
})
export class AdminModule { }
