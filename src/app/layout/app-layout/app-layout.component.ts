import { Component, OnInit } from '@angular/core';
import { User } from '../../shared/models';
declare var $;
import { AuthService } from '../../shared/services';

@Component({
  selector: 'app-app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.css'],
  providers:[AuthService]
})
export class AppLayoutComponent implements OnInit {
  CurrentUser: User;
  constructor(private _user: AuthService) {
    this._user.ActiveUser.subscribe(e => this.CurrentUser = e);
  }
  ngOnInit() {
    setTimeout(() => {
      $("a.c-sidebar__link").click(function (e) {
        $("a.c-sidebar__link:not(e)").removeClass("active");
        $(this).addClass("active");
      });
    }, 2000);
  }
}
