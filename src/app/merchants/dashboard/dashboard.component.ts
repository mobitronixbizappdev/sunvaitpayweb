import { Component, OnInit } from '@angular/core';
declare var CanvasJS;
declare var $;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class MDashboardComponent implements OnInit {

  constructor() {

  }

  ngOnInit() {
    var chart = new CanvasJS.Chart("chartContainer", {
      animationEnabled: true,
      theme: "light2",
      axisY: {
        includeZero: false
      },
      data: [{
        type: "line",
        dataPoints: [
          { y: 450 },
          { y: 414 },
          { y: 520, indexLabel: "highest", markerColor: "red", markerType: "triangle" },
          { y: 460 },
          { y: 450 },
          { y: 500 },
          { y: 480 },
          { y: 480 },
          { y: 410, indexLabel: "lowest", markerColor: "DarkSlateGrey", markerType: "cross" },
          { y: 500 },
          { y: 480 },
          { y: 510 }
        ]
      }]
    });
    chart.render();
    $.getScript("assets/js/onboard.js");
    $("a.c-sidebar__link:not(e)").removeClass("active");
    $("#liDashboard").addClass("active");
  }

}
