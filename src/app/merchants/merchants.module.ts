import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Angular2PromiseButtonModule } from 'angular2-promise-buttons';
import { MSignupComponent, MLoginComponent, MDashboardComponent, MRolesComponent, MUsersComponent, MSettingsComponent} from './index';

@NgModule({
  imports: [
    CommonModule, Angular2PromiseButtonModule.forRoot(),RouterModule
  ],
 /*  declarations: [MLoginComponent],
  exports: [MLoginComponent]*/
 declarations: [MSignupComponent, MLoginComponent, MUsersComponent, MSettingsComponent, MDashboardComponent, MRolesComponent],
  exports: [MSignupComponent, MLoginComponent, MUsersComponent, MSettingsComponent, MDashboardComponent, MRolesComponent]
})
export class MerchantsModule { }
