import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class MSettingsComponent implements OnInit {
  public accountType: string;

  constructor() { }

  ngOnInit() {
  }

  checkInputValue(event: any) {
    this.accountType = (<HTMLInputElement>event.target).value;
  }
}
