import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router , ActivatedRoute} from '@angular/router';
import { AuthService } from '../../shared/services';
import { SignupCredentials } from '../../shared/models';
declare var $; declare var iziToast;
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [AuthService]
})
export class MSignupComponent implements OnInit, OnDestroy {
  psb: any; signupData: SignupCredentials; returnUrl: string = null;
  pNotify = undefined;
  constructor(private router: Router, private route: ActivatedRoute, private _auth: AuthService) {
     this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'merchant/dashboard';
  }

  ngOnInit() {
    $('body').addClass('u-bg-dull-white');
  }

  ngOnDestroy() {
    $('body').addClass('u-bg-dull-white');
  }
  register() {
    this.psb = new Promise((resolve, reject) => {
      this._auth.MerchantSignup(this.signupData)
        .subscribe(
          data => {
            const res: any = data;
            if (res.success) {
              localStorage.setItem('currentUser', JSON.stringify(res.data));
              localStorage.setItem('token', res.Token);
              iziToast.info({
                title: 'Welcome To SunvaitPay!',
                position: 'topCenter',
                timeout: 20000,
              });
              location.href = '#/merchant/settings' ;
            } else {
              iziToast.error({
                title: 'Please Fill All Fields.',
                position: 'topCenter',
                timeout: 20000,
              });
              setTimeout(resolve, 1000);
            }
          },
         error => {
           iziToast.error({
            title: 'Something went wrong, please try again',
            position: 'topCenter',
            timeout: 20000,
          });
            setTimeout(resolve, 1000);
          });
    });
  }
}
