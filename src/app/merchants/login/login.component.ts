import { Component, OnInit , OnDestroy} from '@angular/core';
import { Router , ActivatedRoute} from '@angular/router';
import { AuthService } from '../../shared/services';
import { LoginCredentials } from '../../shared/models';
declare var $; declare var iziToast;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthService]
})
export class MLoginComponent implements OnInit, OnDestroy {
  psb: any; loginData: LoginCredentials; returnUrl: string = null;
  pNotify = undefined;
  constructor(private router: Router, private route: ActivatedRoute, private _auth: AuthService) {
     this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'merchant/dashboard';
  }

  ngOnInit() {
    $('body').addClass('u-bg-dull-white');
  }

  ngOnDestroy() {
    $('body').addClass('u-bg-dull-white');
  }
  login() {
    this.psb = new Promise((resolve, reject) => {
      this._auth.MerchantLogin(this.loginData)
        .subscribe(
          data => {
            const rs: any = data;
            if (rs.success) {
              localStorage.setItem('currentUser', JSON.stringify(rs.data));
              localStorage.setItem('token', rs.Token);
              iziToast.info({
                title: 'Welcome Back',
                position: 'topCenter',
                timeout: 20000,
              });
              location.href = '#/' + this.returnUrl;
              // this.router.navigateByUrl(this.returnUrl);
            } else {
              iziToast.error({
                title: 'Invalid Username or Password',
                position: 'topCenter',
                timeout: 20000,
              });
              setTimeout(resolve, 1000);
            }
          },
         error => {
           iziToast.error({
            title: 'Please try again',
            position: 'topCenter',
            timeout: 20000,
          });
            setTimeout(resolve, 1000);
          });
    });
  }
}
