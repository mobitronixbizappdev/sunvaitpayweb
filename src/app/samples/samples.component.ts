import { Component, OnInit } from '@angular/core';
import PNotify from 'pnotify/dist/es/PNotify';
import PNotifyButtons from 'pnotify/dist/es/PNotifyButtons';
@Component({
  selector: 'app-samples',
  templateUrl: './samples.component.html',
  styleUrls: ['./samples.component.css']
})
export class SamplesComponent implements OnInit {

  constructor() { 
    PNotifyButtons; 
  }

  ngOnInit() {
  }
  ShowAlert(type:number){
    if(type==0){
    PNotify.alert('Notice me, I am an alert!');
    }
    else if(type==1){
    PNotify.success('Notice me, I am an alert!');
    }
    else if(type==2){
    PNotify.error('Notice me, I am an alert!');
    }
    else if(type==3){
    PNotify.error('Notice me, I am an alert!');
    }
    else if(type==4){
    PNotify.Notice('Notice me, I am an alert!');
    }
    
  }

}
