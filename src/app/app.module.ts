import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import {routing} from './app.routing';
import { Ng2IziToastModule } from 'ng2-izitoast';

import { AppComponent } from './app.component';
import { AppLayoutComponent } from './layout/app-layout/app-layout.component';
import { SamplesComponent } from './samples/samples.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './shared/guards';
import { AuthService } from './shared/services';
import { DataService } from './shared/services/data.service';
import {Angular2PromiseButtonModule} from 'angular2-promise-buttons/dist';
import { Configuration } from './app.constant';

import { MerchantsModule } from './merchants/merchants.module';
import {AdminModule} from './admin/admin.module';

@NgModule({
  declarations: [
    AppComponent,
    SamplesComponent,
    AppLayoutComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, routing, AdminModule, Angular2PromiseButtonModule.forRoot(), Ng2IziToastModule, MerchantsModule
  ],
  providers: [AuthGuard, DataService, Configuration],
  bootstrap: [AppComponent]
})
export class AppModule { }
